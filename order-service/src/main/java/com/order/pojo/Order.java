package com.order.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.*;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Builder
@TableName("tb_order")
public class Order {

    @TableId(value ="id",type= IdType.AUTO)
    private Long id;
    private String ordernumber;
    private Double price;
    private String img;
    private Date createTime;
    private String username;
    private Long userId;
    private Long productId;



}
