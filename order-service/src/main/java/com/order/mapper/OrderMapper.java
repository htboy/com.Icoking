package com.order.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.order.pojo.Order;

/**
 * @author Administrator
 */
public interface OrderMapper extends BaseMapper<Order> {

}
