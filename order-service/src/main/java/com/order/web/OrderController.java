package com.order.web;

import com.order.dto.UserDto;
import com.order.pojo.Order;
import com.order.service.OrderService;
import com.order.service.UserServiceInterFace;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Random;

@Controller
@RequestMapping("/order/")
public class OrderController {

    @Autowired
    private OrderService orderService;

    @Autowired
    private DiscoveryClient discoveryClient;

    @Autowired
    public UserServiceInterFace userser;

    /**
     * 采用模板调用
     * @param productId
     * @param userId
     * @return
     */
    @GetMapping("savaOrder")
    @ResponseBody
    public Order saveOrder(Long productId, Long userId ){
      return  orderService.makeOder(productId,userId);

    }

    /**
     * 采用openfeign调用
     * @return
     */
    @GetMapping("getUser")
    @ResponseBody
    public UserDto getUser(){
        //随机获取
        Random r=new Random();
        int n=r.nextInt(3)+1;
        Long id=Long.valueOf(n);
        return userser.getUser(id);
    }

    @GetMapping("getOrder")
    @ResponseBody
    public List<Order> getOrder(Long userId ){
        return orderService.getOrder(userId);
    }

    @GetMapping("test")
    @ResponseBody
    public List<String> gettest(){
        return discoveryClient.getServices();
    }

    @GetMapping("test2")
    @ResponseBody
    public List<ServiceInstance> gettests(){
        return discoveryClient.getInstances("user-service");
    }


}
