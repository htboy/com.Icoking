package com.order.service;

import com.order.pojo.Order;

import java.util.List;

public interface OrderService {
    /**
     *
     * @param productId
     * @param userId
     */
    public Order makeOder(Long productId,Long userId);

    /**
     * 根据用户查询商品
     * @return
     */
    public List<Order> getOrder(Long userId);
}
