package com.order.service;

import com.order.dto.UserDto;
import com.order.service.fallback.UserServiceFallBack;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;


/**
 * @author Administrator
 */
@FeignClient(value="user-service",fallback = UserServiceFallBack.class)
public interface UserServiceInterFace {

    /**
     * 获取用户
     * @param id
     * @return
     */
    @GetMapping("/user/user")
    public UserDto getUser(@RequestParam(value = "id") Long id );
}
