package com.order.service.fallback;

import com.order.dto.UserDto;
import com.order.service.UserServiceInterFace;
import org.springframework.stereotype.Component;

/*
 *获取用户回调类
 * @author Administrator
 */
@Component
public class UserServiceFallBack implements UserServiceInterFace {

    /**
     *
     * @param id
     * @return
     */
    @Override
    public UserDto getUser(Long id) {
        UserDto userDto =new UserDto();
        System.out.println("回调函数userDto = "+ userDto);
        return userDto;
    }
}
