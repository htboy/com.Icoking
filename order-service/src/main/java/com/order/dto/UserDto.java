package com.order.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.util.Date;

@Data
public class UserDto {

    private Long id;
    private String userName;
    private String password;
    private String name;
    private String note;
    private Integer age;
    private Date createTime;
    private Date brithday;
    private Integer sex;
    private Date updateTime;
}
