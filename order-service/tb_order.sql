/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50717
Source Host           : localhost:3306
Source Database       : order-center-db

Target Server Type    : MYSQL
Target Server Version : 50717
File Encoding         : 65001

Date: 2020-11-12 22:29:15
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tb_order
-- ----------------------------
DROP TABLE IF EXISTS `tb_order`;
CREATE TABLE `tb_order` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ordernumber` varchar(64) DEFAULT NULL,
  `price` double(4,2) DEFAULT NULL,
  `img` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `product_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='订单';

-- ----------------------------
-- Records of tb_order
-- ----------------------------
INSERT INTO `tb_order` VALUES ('1', '225e4691-e708-42fd-b619-e7fbf221d44e', '10.30', 'aaaa.jpg', '2020-11-11 21:13:44', 'aaa', '1', '1');
INSERT INTO `tb_order` VALUES ('2', '9928abe4-1859-4004-a4d0-309903c039c2', '10.30', 'aaaa.jpg', '2020-11-12 22:06:05', 'zhangsan', '2', '1');
INSERT INTO `tb_order` VALUES ('3', 'a8406f6d-cc6f-480a-af33-1ec6b41bb95a', '10.30', 'aaaa.jpg', '2020-11-12 22:17:40', 'zhangsan', '2', '1');
INSERT INTO `tb_order` VALUES ('4', '41d0da3e-0215-4f45-83a1-5138eb8c7747', '10.30', 'aaaa.jpg', '2020-11-12 22:17:44', 'zhangsan', '1', '1');
INSERT INTO `tb_order` VALUES ('5', '37a4ce7a-bc46-4c28-a1a9-cff796706fe2', '10.30', 'aaaa.jpg', '2020-11-12 22:17:46', 'zhangsan', '3', '1');
INSERT INTO `tb_order` VALUES ('6', '020a9647-5894-4944-8dd5-d4a2682924d8', '10.30', 'aaaa.jpg', '2020-11-12 22:17:47', 'zhangsan', '1', '1');
INSERT INTO `tb_order` VALUES ('7', '7be398ef-fdc9-4804-a437-24e331d9db5e', '10.30', 'aaaa.jpg', '2020-11-12 22:17:50', 'zhangsan', '3', '1');
INSERT INTO `tb_order` VALUES ('8', 'f4b38db2-37b3-402a-8a78-4611f16ea66d', '10.30', 'aaaa.jpg', '2020-11-12 22:19:10', 'zhangsan', '3', '1');
INSERT INTO `tb_order` VALUES ('9', 'b3f1cceb-3a7f-4b06-92f5-76991890a28a', '10.30', 'aaaa.jpg', '2020-11-12 22:20:03', 'zhangsan', '3', '1');
