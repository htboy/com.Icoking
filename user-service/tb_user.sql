/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50717
Source Host           : localhost:3306
Source Database       : user-center-db

Target Server Type    : MYSQL
Target Server Version : 50717
File Encoding         : 65001

Date: 2020-11-12 22:29:27
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tb_user
-- ----------------------------
DROP TABLE IF EXISTS `tb_user`;
CREATE TABLE `tb_user` (
  `id` bigint(20) NOT NULL,
  `user_name` varchar(64) DEFAULT NULL,
  `password` varchar(64) DEFAULT NULL,
  `age` int(2) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `brithday` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `sex` int(1) DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户';

-- ----------------------------
-- Records of tb_user
-- ----------------------------
INSERT INTO `tb_user` VALUES ('1', 'zhangsan', '123456', '30', '张三', '2020-11-11 20:59:11', '2000-01-11 20:59:16', '2020-11-11 20:59:31', '1', '张三同学在测试');
INSERT INTO `tb_user` VALUES ('2', 'lisi', '123456', '20', '李四', '2020-11-12 22:21:05', '1999-02-10 22:21:08', '2020-11-12 22:21:21', '0', '李四同学在睡觉');
INSERT INTO `tb_user` VALUES ('3', 'jiaodizhu', '456789', '40', '叫地主', '2020-11-12 22:22:15', '1980-11-12 22:22:20', '2020-11-30 22:22:33', '1', '王炸');
