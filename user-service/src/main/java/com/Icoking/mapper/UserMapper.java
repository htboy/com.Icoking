package com.Icoking.mapper;

import com.Icoking.pojo.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @author Administrator
 */
public interface UserMapper extends BaseMapper<User> {
}
