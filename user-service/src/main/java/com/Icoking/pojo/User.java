package com.Icoking.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.*;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Builder
@TableName("tb_user")
public class User {
    @TableId(value ="id",type= IdType.AUTO)
    private Long id;
    private String userName;
    private String password;
    private String name;
    private String note;
    private Integer age;
    private Date createTime;
    private Date brithday;
    private Integer sex;
    private Date updateTime;

}
