package com.Icoking.service.impl;

import com.Icoking.mapper.UserMapper;
import com.Icoking.pojo.User;
import com.Icoking.service.UserService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Administrator
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    public User getUser(Long id) {

        User order=User.builder().id(id).build();
            QueryWrapper<User> queryWrapper = new QueryWrapper<User>(order);
            List<User> list=userMapper.selectList(queryWrapper);
            System.out.println("请求到数据 = "+ list.get(0));
            return list.get(0);
    }
}
