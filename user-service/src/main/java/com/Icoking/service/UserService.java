package com.Icoking.service;

import com.Icoking.pojo.User;

public interface UserService {

    /**
     * 获取用户信息
     * @param id
     * @return
     */
    public User getUser( Long id);
}
