package com.Icoking.web;

import com.Icoking.pojo.User;
import com.Icoking.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/user/")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("user")
    @ResponseBody
    public User getUser(@RequestParam(value = "id") Long id ){
        return userService.getUser(id);
    }
}
